# Sensitive information in GET request

| **Likelihood** | **Consequence** | **Overall risk** |
| -------------- | --------------- | ---------------- |
| Possible       | Moderate        | **Medium Risk**  |

## Description

The web server generates a session token when a user logs in which is used to authenticate from then on. However, the session token is sent as a parameter within a GET request which insecurely exposes it. 

A session token should never be passed as a parameter in a GET request because:

-       Web server logs will store the entire GET request and anyone that has access to web server logs will have the session tokens needed to authenticate as the user.
-       When a user clicks on a link to another website from {{ CLIENT }}’s website, the browser will add the referrer to the request headers and expose the session token to the other website. 
-       The browser history will contain the entire GET request, including the session token used to authenticate. Anyone with access to the same computer as the user will be able to access the session token.
-       Page print outs will contain the entire page URL including the session token.
-       Users may copy and paste links to others without knowing they contain the sensitive session token.

The following hosts disclose session tokens in a GET request:

| **Page** | **Link** |
| -------- | -------- |
|          |          |

**Risk Assessment**

Persons with additional levels of access, such as web administrators or people with access to the same computer as the user, would have the opportunity to view the user’s session ID. Successful exploitation is likely to result in stolen session ids and the compromise of the victim’s account.

Recommendation

Session IDs should be transferred in the body of a POST request.